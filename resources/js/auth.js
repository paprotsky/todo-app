class Auth {
  constructor() {
    this.token = window.localStorage.getItem('token')
    // console.log(this.token)
    let userData = window.localStorage.getItem('user')
    this.user = userData ? JSON.parse(userData) : null
    // this.user = null

    if (this.token) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.token

      this.getUser()
    }
  }

  check () {
    return !! this.token
  }

  getUser() {
    api.call('get', '/api/get-user')
      .then(({data}) => {
        this.user = data
      })
  }

  login (token, user) {
    window.localStorage.setItem('token', token);
    window.localStorage.setItem('user', JSON.stringify(user));

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

    this.token = token;
    this.user = user;

    Event.$emit('userLoggedIn');
  }

  logout () {
    axios.defaults.headers.common['Authorization'] = 'Bearer '

    window.localStorage.removeItem('token')
    window.localStorage.removeItem('user')
  }
}

export default Auth;
