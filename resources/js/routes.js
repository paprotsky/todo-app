import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/js/components/Home'
import Login from '@/js/components/Login'
import Register from '@/js/components/Register'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/404',
      name: '404',
      component: require('@/js/components/404')
    },
    {
      path: '*',
      redirect: '404',
    },
  ]
})

export default router
