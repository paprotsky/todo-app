
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

import Vue from 'vue'
import Vuetify from 'vuetify'

import App from '@/js/views/App'
import Routes from '@/js/routes.js'

import Auth from './auth.js';
import Api from './api.js';

window.api = new Api();
window.auth = new Auth();
window.Event = new Vue;

Vue.use(Vuetify)

const a = new Vue({
  el: '#app',
  router: Routes,
  render: h => h(App)
})

export default app

